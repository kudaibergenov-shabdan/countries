import React, {useCallback, useEffect, useState} from 'react';
import './CountryItemInfo.css';
import axios from "axios";
import {EXACT_COUNTRY_INFO_BY_ALPHA_CODE_URL, EXACT_COUNTRY_INFO_BY_NAME_URL} from "../../config";

const CountryItemInfo = ({additionalClasses, selectedCountry}) => {
    let baseClass = ['CountryItemInfo'];
    if (additionalClasses) {
        baseClass = baseClass.concat(additionalClasses);
    }

    const [country, setCountry] = useState(null);
    const [borders, setBorders] = useState([]);

    const fetchCountryInfo = useCallback(async () => {
        if (selectedCountry === null) return;
        const response = await axios.get(EXACT_COUNTRY_INFO_BY_NAME_URL + selectedCountry);
        const countryInfo = response.data[0];
        setCountry(countryInfo);

        const responses =
            await Promise.all(countryInfo.borders.map(
                countryAlphaCode => axios.get(EXACT_COUNTRY_INFO_BY_ALPHA_CODE_URL + countryAlphaCode)));
        setBorders(responses);
    }, [selectedCountry]);

    useEffect(() => {
        fetchCountryInfo().catch(error => console.error('[CountryItemInfo]', error));
    }, [fetchCountryInfo]);

    return (
        <div className={baseClass.join(' ')}>
            {country
                ? (
                    <>
                        <div className="country-main-info">
                            <p className="country-flag">
                                <img
                                    className="country-flag-img"
                                    src={country.flag}
                                    alt={country.name + ' flag'}
                                />
                            </p>
                            <p className="country-title country-name">{country.name}</p>
                            <p className="country-title country-capital">Capital: {country.capital}</p>
                            <p className="country-title country-population">Population: {country.population}</p>
                        </div>
                        <div className="country-additional-info">
                            <p className="country-border-title">Borders with:</p>
                            {
                                (borders.length > 0 ) ? (
                                    <ul className="country-borders-list">
                                        {borders.map(border => (
                                            <li key={border.data.name}>{border.data.name}</li>
                                        ))}
                                    </ul>
                                ) : <p className="country-border-title country-no-border">No borders</p>
                            }

                        </div>
                    </>
                )
                : (<p className="choose-country-title">&nbsp;Выберите страну из списка</p>)
            }
        </div>
    );
};

export default CountryItemInfo;
import React from 'react';
import './CountryList.css';
import CountryListItem from "../CountryListItem/CountryListItem";

const CountryList = props => {
    let baseClass = ['CountryList'];
    if (props.additionalClasses) {
        baseClass = baseClass.concat(props.additionalClasses);
    }

    return (
        <div className={baseClass.join(' ')}>
            {props.countries.map(country => (
                <CountryListItem
                    key={country.name}
                    countryName={country.name}
                    onClick={() => props.onClick(country.name)}
                />
            ))}
        </div>
    );
};

export default CountryList;
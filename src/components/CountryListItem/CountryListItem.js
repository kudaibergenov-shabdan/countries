import React from 'react';
import './CountryListItem.css';

const CountryListItem = ({countryName, onClick}) => {
    return (
        <>
            <p className="country-item-paragraph" onClick={onClick}>
                <span className="country-item-text">{countryName}</span>
            </p>
        </>
    );
};

export default CountryListItem;
import React, {useEffect, useState} from 'react';
import './CountryBlock.css';
import CountryList from "../../components/CountryList/CountryList";
import CountryItemInfo from "../../components/CoutryItemInfo/CoutryItemInfo";
import axios from "axios";
import {ALL_COUNTRY_INFO_URL} from "../../config";

const CountryBlock = () => {
    const [countries, setCountries] = useState([]);
    const [selectedCountry, setSelectedCountry] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(ALL_COUNTRY_INFO_URL);
            setCountries(response.data);
        };
        fetchData().catch(e => console.error('Error in fetching country list.', e));
    }, []);

    return (
        <div className="CountryBlock">
            <CountryList
                additionalClasses={['box']}
                countries={countries}
                onClick={countryName => setSelectedCountry(countryName)}
            />
            <CountryItemInfo
                additionalClasses={['box']}
                selectedCountry={selectedCountry}
            />
        </div>
    );
};

export default CountryBlock;
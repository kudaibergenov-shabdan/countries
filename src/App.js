import CountryBlock from "./containers/CountryBlock/CountryBlock";

const App = () => (
    <div className="App">
        <CountryBlock />
    </div>
);

export default App;

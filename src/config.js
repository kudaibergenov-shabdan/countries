export const ALL_COUNTRY_INFO_URL = 'all';
export const EXACT_COUNTRY_INFO_BY_NAME_URL = 'name/';
export const EXACT_COUNTRY_INFO_BY_ALPHA_CODE_URL = 'alpha/';